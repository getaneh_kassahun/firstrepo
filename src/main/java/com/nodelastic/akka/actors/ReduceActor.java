package com.nodelastic.akka.actors;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import com.nodelastic.akka.model.messages.MapData;
import com.nodelastic.akka.model.messages.ReduceData;
import com.nodelastic.akka.model.messages.WordCount;

import java.util.HashMap;
import java.util.List;

/**
 * Class ReduceActor
 *
 * @author Vadim Vera
 * @version 0.1
 */
public class ReduceActor extends UntypedActor {
    private ActorRef aggregateActor;

    public ReduceActor(ActorRef aggregateActor) {
        this.aggregateActor = aggregateActor;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof MapData) {
            MapData mapData = (MapData)message;
            ReduceData reduceData = reduce(mapData.getDataList()); // reduce the incoming data

            aggregateActor.tell(reduceData); // forward the result to aggregate actor
        } else {
            unhandled(message);
        }
    }

    private ReduceData reduce(List<WordCount> dataList) {
        HashMap<String, Integer> reducedMap = new HashMap();

        for (WordCount wordCount : dataList) {
            if (reducedMap.containsKey(wordCount.getWord())) {
                Integer value = reducedMap.get(wordCount.getWord());

                value++;
                reducedMap.put(wordCount.getWord(), value);
            } else {
                reducedMap.put(wordCount.getWord(), Integer.valueOf(1));
            }
        }
        return (new ReduceData(reducedMap));
    }
}

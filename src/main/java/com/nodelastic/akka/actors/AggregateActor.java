package com.nodelastic.akka.actors;

import akka.actor.UntypedActor;
import com.nodelastic.akka.model.messages.ReduceData;
import com.nodelastic.akka.model.messages.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Class AggregateActor
 * 
 * @author Vadim Vera
 * @version 0.1
 */
public class AggregateActor extends UntypedActor {
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	private Map<String, Integer> reducedMap = new HashMap<>();

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof ReduceData) {
			ReduceData reduceData = (ReduceData) message;
			System.out.println("getaneh");
			aggregateInMemoryReduce(reduceData.getReduceDataList());
		} else if (message instanceof Result) {
			LOGGER.info(reducedMap.toString());
		} else {
			unhandled(message);
		}
	}

	private void aggregateInMemoryReduce(Map<String, Integer> reducedList) {
		Integer count;

		for (String key : reducedList.keySet()) {
			if (reducedMap.containsKey(key)) {
				count = reducedList.get(key) + reducedMap.get(key);
				reducedMap.put(key, count);
			} else {
				reducedMap.put(key, reducedList.get(key));
			}
		}
	}
}

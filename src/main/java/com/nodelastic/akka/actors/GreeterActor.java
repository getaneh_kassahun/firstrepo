package com.nodelastic.akka.actors;

import akka.actor.UntypedActor;
import com.nodelastic.akka.model.messages.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.nodelastic.akka.model.messages.Message.Done;
import static com.nodelastic.akka.model.messages.Message.Greet;

/**
 * Class GreeterActor
 *
 * @author Vadim Vera
 * @version 0.1
 */
public class GreeterActor extends UntypedActor {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public void onReceive(Object message) {
        if (message instanceof Message) {
            switch ((Message)message) {
                case Greet: {
                    LOGGER.info(Greet.getMessage());
                    break;
                }
                case Done: {
                    LOGGER.info(Done.getMessage());
                    break;
                }
            }
            getSender().tell(Done, getSelf());
        } else {
            unhandled(message);
        }
    }
}

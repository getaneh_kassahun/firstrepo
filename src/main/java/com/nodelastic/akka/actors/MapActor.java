package com.nodelastic.akka.actors;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import com.nodelastic.akka.model.messages.MapData;
import com.nodelastic.akka.model.messages.WordCount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Class MapActor
 *
 * @author Vadim Vera
 * @version 0.1
 */
public class MapActor extends UntypedActor {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private final String[] STOP_WORDS = { "a", "am", "an", "and", "are", "as", "at", "be", "do", "go", "if", "in", "is",
            "it", "of", "on", "the", "to" };
    private List<String> STOP_WORDS_LIST = Arrays.asList(STOP_WORDS);
    private ActorRef reduceActor;

    public MapActor(ActorRef reduceActor) {
        this.reduceActor = reduceActor;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof String) {
            String work = (String)message;
            MapData data = evaluateExpression(work); // map the words in the sentence

            reduceActor.tell(data); // send the result to ReduceActor
        } else { unhandled(message); }
    }

    private MapData evaluateExpression(String line) {
        List<WordCount> dataList = new ArrayList<>();
        StringTokenizer parser = new StringTokenizer(line);

        while (parser.hasMoreTokens()) {
            String word = parser.nextToken().toLowerCase();

            if (!STOP_WORDS_LIST.contains(word)) {
                dataList.add(new WordCount(word, Integer.valueOf(1)));
            }
        }
        return (new MapData(dataList));
    }
}

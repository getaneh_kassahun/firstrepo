package com.nodelastic.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.nodelastic.akka.actors.MasterActor;
import com.nodelastic.akka.model.messages.Result;

/**
 * Class MapReduceApplication
 *
 * @author Vadim Vera
 * @version 0.1
 */
public class MapReduceApplication {
    public static void main(String[] args) throws Exception {
        ActorSystem system = ActorSystem.create("MapReduceApplication");
        ActorRef master = system.actorOf(new Props(MasterActor.class), "master");

        master.tell("The quick brown fox tried to jump over the lazy dog and fell on the dog");
        master.tell("Dog is man's best friend");
        master.tell("Dog and Fox belong to the same family");

        Thread.sleep(500);

        master.tell(new Result());

        Thread.sleep(500);

        system.shutdown();
    }
}

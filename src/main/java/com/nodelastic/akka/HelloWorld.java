package com.nodelastic.akka;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.nodelastic.akka.actors.GreeterActor;
import com.nodelastic.akka.model.messages.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.nodelastic.akka.model.messages.Message.Done;
import static com.nodelastic.akka.model.messages.Message.Greet;

/**
 * Class HelloWorld
 *
 * @author Vadim Vera
 * @version 0.1
 */
public class HelloWorld extends UntypedActor {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public void onReceive(Object message) {
        if (message instanceof Message) {
            switch ((Message)message) {
                case Greet: {
                    LOGGER.info(Greet.getMessage());
                    break;
                }
                case Done: {
                    LOGGER.info(Done.getMessage());
                    break;
                }
            }
            getContext().stop(getSelf());
        } else {
            unhandled(message);
        }
    }

    @Override
    public void preStart() {
        LOGGER.info("Inside preStart()...");

        // create the greeter actor
        final ActorRef greeter = getContext().actorOf(Props.create(GreeterActor.class), "greeter");
        // tell it to perform the greeting
        greeter.tell(Greet, getSelf());
    }

    @Override
    public void postStop() {
        LOGGER.info("Inside postStop()...");
    }
    public void sayHello(){
    	System.out.println("Hello from Yoseph!");
    }
}

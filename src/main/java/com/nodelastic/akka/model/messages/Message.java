package com.nodelastic.akka.model.messages;

/**
 * Class Message
 *
 * @author Vadim Vera
 * @version 0.1
 */
public enum Message {
    Greet("Hello World!"),
    Done("Done!");
    private String message;

    private Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

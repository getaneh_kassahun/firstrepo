package com.nodelastic.akka.model.messages;

import java.util.HashMap;

/**
 * Class ReduceData
 *
 * @author Vadim Vera
 * @version 0.1
 */
public class ReduceData {
    private HashMap<String, Integer> reduceDataList;

    public ReduceData(HashMap<String, Integer> reduceDataList) {
        this.reduceDataList = reduceDataList;
    }

    public HashMap<String, Integer> getReduceDataList() {
        return reduceDataList;
    }
}

package com.nodelastic.akka.model.messages;

import java.util.List;

/**
 * Class MapData
 *
 * @author Vadim Vera
 * @version 0.1
 */
public class MapData {
    private List<WordCount> dataList;

    public MapData(List<WordCount> dataList) {
        this.dataList = dataList;
    }

    public List<WordCount> getDataList() {
        return dataList;
    }
}
